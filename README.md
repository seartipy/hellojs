###Install Node

####Mac

Install homebrew

    ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
    brew doctor

Install node using brew

    brew install node

####Linux(Ubuntu)

    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update && sudo apt-get upgrade -y
    sudo apt-get install nodejs


####Windows

Install chocolatey by pasting the following command in command prompt

	@powershell -NoProfile -ExecutionPolicy unrestricted -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%systemdrive%\chocolatey\bin

Install nodejs by pasting the following command in command prompt.

	cinst nodejs
	
###Install bower, grunt and karma

    npm install -g bower grunt-cli karma

###Install git

####Mac

    brew install git

####Linux

    sudo apt-get install git

####Windows
		cinst msysgit

###Setup JS Project

Clone this repository

    git clone https://seartipy@bitbucket.org/seartipy/hellojs.git

Install npm dependencies

    cd hellojs
    npm install

Start the server(watches files,  livereloads, starts static connect server)

    grunt

Open url *https://localhost:8000/index.html*
