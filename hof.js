var repeat = function(n, f) {
  for(var i = 0; i < n; ++i)
    f();
};

var range = function(start, stop) {
  var result = [];
  for(var i = start; i < stop; ++i)
    result.push(i);
  return result;
};

var each = function(arr, f) {
  for(var i = 0; i < arr.length; ++i)
    f(arr[i]);
};

var map = function(arr, f) {
  var mapped = [];
  each(arr, function(e) { mapped.push(f(e)); });
  return mapped;
};

var filter = function(arr, pred) {
  var filtered = [];
  each(arr, function(e) {
    if(pred(e))
      filtered.push(e);
  });

  return filtered;
};

var contains = function(arr, pred) {
  for(var i = 0; i < arr.length; ++i)
    if(pred(arr[i]))
       return true;
  return false;
};

var keys = function(obj) {
  var keys = [];
  for(var prop in obj)
    if(obj.hasOwnProperty(prop))
      keys.push(prop);
  return keys;
};
