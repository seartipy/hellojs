var counter = function(init) {
  var count = init;
  return function() {
    var result = count;
    ++count;
    return result;
  };
};

var create_point = function(x, y) {
  var getX = function() { return x; };
  var getY = function() { return y; };
  var distance = function() {
      return Math.sqrt(x*x + y*y);
  };
  var moveBy = function(dx, dy) {
      x += dx;
      y += dy;
  };

  return {
    getX: getX,
    getY: getY,
    distance: distance,
    moveBy: moveBy
  };
};

var pt = create_point(1, 2);
